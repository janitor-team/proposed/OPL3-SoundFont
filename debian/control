Source: opl3-soundfont
Section: sound
Priority: optional
Maintainer: Bret Curtis <psi29a@gmail.com>
Build-Depends: debhelper (>= 11)
Standards-Version: 4.2.1
Homepage: https://github.com/Mindwerks/opl3-soundfont
Vcs-Git: https://salsa.debian.org/psi29a-guest/OPL3-SoundFont.git
Vcs-Browser: https://salsa.debian.org/psi29a-guest/OPL3-SoundFont

Package: opl3-soundfont
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Enhances: wildmidi, fluidsynth, timidity, musescore
Provides: sf2-soundfont-gm, sf3-soundfont-gm
Description: OPL3 SoundFont that simulates the sound of an OPL3 chip
 This package provides a soundfont (sf2) that can be used by FluidSynth,
 Timidity and WildMidi to play MIDI (and MIDI-like files) using the samples
 created by the OPL3 (SB16/YM262) chip. This also allows applications like
 DOSBox, ZDoom and others to play back MIDI without having to emulate the
 OPL3, which can be costly in terms of CPU usage to emulate accurately.
 .
 This package will be installed into /usr/share/sounds/sf2/.
